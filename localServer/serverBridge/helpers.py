import datetime
import json
import os

from django.utils import timezone

from customLibrary.settings import CONTENT_TYPE_TAG, ORIGINAL_URL_TAG, COMPARATOR_STR, LOCAL_SERVER_URL
from customLibrary.storage_utils import is_local_file_necessary, write_device_info, file_path_from_directory_key
from localServer import debugFileLog


def string_to_dict(str):
    return json.loads(str)


def replace_url_in_json(json_data):
    for key in json_data.keys():
        if key == "url" and is_local_file_necessary(json_data[CONTENT_TYPE_TAG]):
            # creating extra tag and modifying 'url' tag so that angular player is abstracted of local server
            json_data[ORIGINAL_URL_TAG] = json_data[key]
            json_data[key] = get_local_media_url(json_data[key], json_data["content_id"])
        elif type(json_data[key]) is dict:
            replace_url_in_json(json_data[key])
        elif type(json_data[key]) is list:
            for element in json_data[key]:
                replace_url_in_json(element)


def get_local_media_url(url, content_id):
    try:
        relative_folder_path = url[url.index(COMPARATOR_STR) + len(COMPARATOR_STR) : url.rindex('/')]
    except Exception as e:
        debugFileLog.error("Exception while forming relative_file_path")
        relative_folder_path = ''
    old_filename = os.path.basename(url)
    old_title, ext = os.path.splitext(old_filename)
    new_title = old_title + '_' + str(content_id)
    new_url = LOCAL_SERVER_URL + relative_folder_path + '/' + new_title + ext
    return new_url


def write_string_to_file(json_str, file_path):
    json_str = str(json_str)
    try:
        with open(file_path, "w") as f:
            f.write(json_str)
    except Exception as e:
        debugFileLog.exception('Writing to file %s failed with exception %s' % (file_path, str(e)))


def write_screens_data_to_file(json_data, device_key):
    debugFileLog.info('write_screens_data_to_file ')
    file_path = file_path_from_directory_key(directory_key='screens_data_dir', filename=device_key)
    json_str = json.dumps(json_data)
    write_string_to_file(json_str, file_path)
    last_received = datetime_to_string(timezone.now(), '%d%m%Y%H%M%S')
    write_device_info(device_key, last_received)


def write_player_logs_to_file(json_str, device_key):
    debugFileLog.info("write_player_logs_to_file")
    filename = device_key + '_' + datetime_to_string(timezone.now().date(), fmt='%Y%m%d') + '.log'
    file_path = file_path_from_directory_key(directory_key='player_logs_dir', filename=filename)
    write_string_to_file(json_str, file_path)


def string_to_datetime(str, fmt='%d%m%Y%H%M%S'):
    """
    :param fmt: format of the date string can be mentioned
    :param str: Format of the string is "%2d%2m%4Y%2H%2M%2S", example 31012016095455
    :return: dt: python datetime object in the utc timezone
    """
    dt = datetime.datetime.strptime(str,fmt)
    dt = timezone.make_aware(dt, timezone.get_default_timezone())
    return dt


def datetime_to_string(datetime_object, fmt='%d%m%Y%H%M%S'):
    """
    :param datetime_object: python datetimeobject
    :param fmt: format of the date string can be mentioned
    :return: dt: python datetime string
    """
    return datetime_object.strftime(fmt)