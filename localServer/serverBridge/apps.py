from django.apps import AppConfig

from localServer import create_directory_structure, default_checks, create_worker_jobs
from localServer.settings import debugFileLog


class ServerBridgeAppConfig(AppConfig):
    name = 'serverBridge'
    verbose_name = "Server Bridge"

    def ready(self):
        # This function needs to be executed on start of the server
        print 'starting the localServer'
        debugFileLog.info('Starting the localServer')
        create_directory_structure()
        default_checks()
        create_worker_jobs()
