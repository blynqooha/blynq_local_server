import requests
# Create your views here.

from customLibrary.storage_utils import get_device_info_list
from downloadManagement.views import download_media_for_screen
from localServer.settings import debugFileLog
from customLibrary.settings import SCHEDULES_URL
from serverBridge.helpers import string_to_dict, replace_url_in_json, write_screens_data_to_file


def fetch_screen_data(device_key, last_received):
    debugFileLog.info('fetch_screen_data started executing')
    try:
        json_params = {"device_key": device_key, "last_received": last_received}
        response = requests.post(url=SCHEDULES_URL, json=json_params)
        if response.status_code == 200:
            json_content = string_to_dict(response.content)
            return json_content
        else:
            debugFileLog.error("Received status code %d, reason %s" % (response.status_code, response.reason))
    except Exception as e:
        debugFileLog.exception("Exception while fetching screen data from server: %s" % str(e))
    return {}


def download_all_screens_data():
    debugFileLog.info('download_all_screen_data started executing')
    device_info_list = get_device_info_list()
    for screen_info in device_info_list:
        device_key = screen_info[0]
        last_received = screen_info[1]
        download_screen_data(device_key, last_received)


def download_screen_data(device_key, last_received):
    debugFileLog.info('download_screen_data started executing device_key %s last_received %s' % (device_key, last_received))
    original_json_data = fetch_screen_data(device_key, last_received)
    if original_json_data.get('is_modified'):
        replace_url_in_json(original_json_data)  # call by reference
        write_screens_data_to_file(original_json_data, device_key)
        download_media_for_screen(device_key)
