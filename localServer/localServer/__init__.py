import os
import shutil
import threading

from customLibrary import settings as properties
from customLibrary.storage_utils import check_disk_space, disk_usage
from localServer.settings import BASE_DIR, debugFileLog
from serverBridge.views import download_all_screens_data


def create_directory_structure():
    for key in properties.RELATIVE_DIRECTORIES:
        directory = properties.RELATIVE_DIRECTORIES[key]
        absolute_path = os.path.join(BASE_DIR, directory)
        if not os.path.exists(absolute_path):
            os.makedirs(absolute_path)
    debugFileLog.info("Creating directory structure completed successfully")


def default_checks():
    # available disk space above threshold
    clean_temp_directory()
    check_disk_space()
    debugFileLog.info('default_checks completed successfully')


def clean_temp_directory():
    temp_dir = os.path.join(BASE_DIR, properties.RELATIVE_DIRECTORIES['temp_dir'])
    for the_file in os.listdir(temp_dir):
        file_path = os.path.join(temp_dir, the_file)
        try:
            if os.path.isfile(file_path):
                os.remove(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            debugFileLog.exception(e)
    debugFileLog.info('clean_temp_directory completed successfully')


def fetch_screens_data_periodically():
    download_all_screens_data()
    threading.Timer(30, fetch_screens_data_periodically).start()


def create_worker_jobs():
    fetch_screens_data_periodically()