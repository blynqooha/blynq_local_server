(function() {
	'use strict'
	// body...
	/*
		This module(logger app) stores the 
			a.screen activity logs
			b.playlist item logs 
		in respective tables of database 'BlynqDB'
	*/

	var lGApp = angular.module('lGApp',['sDApp'])


	lGApp.config(function($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

     	// $httpProvider.interceptors.push(function ($rootScope, $q) {
	    //     return {
	    //         request: function (config) {
	    //             config.timeout = 10000;
	    //             return config;
	    //         },
	    //         responseError: function (rejection) {
	    //             switch (rejection.status){
	    //                 case 408 :
	    //                     console.log('connection timed out');
	    //                     break;
	    //             }
	    //             return $q.reject(rejection);
	    //         }
	    //     }
    	// })
    });

	//config
    lGApp.service('lGConfig', [function(){
    	//DB
    	this.databaseName = 'blynqDBDummy';

    	//Object Stores/ Tables Names
    	this.OSSessionHistory = 'sessionHistory';
    	this.OSMediaStats = 'mediaStats';
    	this.OSErrorLogs = 'errorLogs';

    	//Object Stores/ Tables Column Names
    	this.sessionHistoryCol = {
    		sent_to_server : 'sent_to_server'
    		,device_key : 'device_key'
    		,session_start_time : 'session_start_time'
    		,session_end_time : 'session_end_time'
    		,date : 'date'
    		,date_time : 'date_time'
    	};
    	this.mediaStatsCol = {
    		playlist_id: 'playlist_id'
    		,content_id : 'content_id'
    		,count : 'count'
    		,time_played : 'time_played'
    		,date : 'date'
    		,device_key : 'device_key'
    		,sent_to_server : 'sent_to_server'
    	};
    	this.errorLogsCol= {
    		device_key : 'device_key'
    		,status_code : 'status_code'
    		,error_text : 'error_text'
    		,date_time : 'date_time'
    		,sent_to_server : 'sent_to_server'
    	};
    	this.sent_to_server = 'sent_to_server';

    	//Reporting URLs
    	this.statsReportingUrl = '/api/player/mediaStats';
    	this.errLogsReportingUrl = '/api/player/logs';

    	//Indexes
    	this.logMediaStatsIndex = 'logMediaStatsIndex';
    	this.mSSentToServerIndex = 'mSSentToServerIndex';
    	this.shSessionLogIndex = 'shSessionLogIndex';
    	this.sHSentToServerIndex = 'sHSentToServerIndex';
    	this.eLSentToServerIndex = 'eLSentToServerIndex'; 

    	// Miscellaneous Config
    	this.tryAgainDelay = 60 * 60 * 1000;

    	return this
    }]);


	//initiate database
	lGApp.factory('dBInitialise', ['lGConfig','$q', function(lGConfig, $q){
		// var dbDeleteRequest1 = window.indexedDB.deleteDatabase('blynqDB');
		// var dbDeleteRequest = window.indexedDB.deleteDatabase(lGConfig.databaseName);
		// dbDeleteRequest.onerror = function(event) {
		//  console.log("Error while deleting database.", true);
		// };

		var request;
		var db;
		var deferredDBInstance = $q.defer();

		var onLoad = function(){
			initiateDBInstance();
		};

		
		var initiateDBInstance = function(){
			request = indexedDB.open(lGConfig.databaseName);
		};
		onLoad();

		request.onupgradeneeded = function(e) {
			// The database did not previously exist, so create object stores and indexes.
			var thisDB = e.target.result;
			if(!thisDB.objectStoreNames.contains(lGConfig.OSSessionHistory)){
				var os = thisDB.createObjectStore(lGConfig.OSSessionHistory , { keyPath: 'id', autoIncrement:true});
				os.createIndex(lGConfig.sHSentToServerIndex, lGConfig.sessionHistoryCol.sent_to_server, {unique : false});
				os.createIndex(lGConfig.shSessionLogIndex, [lGConfig.sessionHistoryCol.date_time, lGConfig.sessionHistoryCol.device_key])
			}

			if(!thisDB.objectStoreNames.contains(lGConfig.OSMediaStats)){
				var os = thisDB.createObjectStore(lGConfig.OSMediaStats , {keyPath: 'id', autoIncrement:true});
				os.createIndex(lGConfig.mSSentToServerIndex, lGConfig.mediaStatsCol.sent_to_server, {unique:false})
				os.createIndex(lGConfig.logMediaStatsIndex, [lGConfig.mediaStatsCol.date, lGConfig.mediaStatsCol.playlist_id, lGConfig.mediaStatsCol.content_id], {unique:false}); 
			}

			if(!thisDB.objectStoreNames.contains(lGConfig.OSErrorLogs)){
				var os = thisDB.createObjectStore(lGConfig.OSErrorLogs , {keyPath: 'id', autoIncrement:true});
				os.createIndex(lGConfig.eLSentToServerIndex, lGConfig.errorLogsCol.sent_to_server, {unique:false});
			}
		};

		request.onsuccess = function() {
		  	db = request.result;
		  	deferredDBInstance.resolve(db);
		  	manageMemory();
		};

		request.onerror = function(){
			console.log('Error while obtaining player-logs-instance');
			//log error
			deferredDBInstance.reject();
		};

		var deleteLogs = function(OSName, indexKey){
			var tx = db.transaction(OSName, "readwrite");
			var store = tx.objectStore(OSName);
			var index = store.index(indexKey);

			var request = index.openCursor(IDBKeyRange.only('true')); 
			request.onsuccess = function() {
				var cursor = request.result;
				if (cursor) {
				    cursor.delete();
				    cursor.continue;
				}
			};
		};

		//public methods
		var getDatabaseInstance = function(){
			if(db){
				return $q.when(db)
			}else{
				return $q.when(deferredDBInstance.promise)
			}	
		};

		var manageMemory = function(){
			//delete the memory whichever it is sent to the server
			deleteLogs(lGConfig.OSSessionHistory, lGConfig.sHSentToServerIndex);
			deleteLogs(lGConfig.OSMediaStats, lGConfig.mSSentToServerIndex);
			deleteLogs(lGConfig.OSErrorLogs, lGConfig.eLSentToServerIndex);
		};

		return{
			getDatabaseInstance : getDatabaseInstance
			,manageMemory : manageMemory
		}
	}]);


	//report logs to server
	lGApp.factory('reportingFactory',['dBInitialise','lGConfig','$http','$q',
		'dateTimeCustomisationsFactory', '$timeout','screenDetailsFactory','errorLoggingFactory',
	 function(dbIF, lGConfig, $http, $q, dTCF,$timeout, sDF, eLF){
		/*
			This factory takes care of sending the data to the server 
			and updating the sentToServer bool with 'true'
		*/
		//var db;

		var sendingInProgress = false;

		var onLoad = function(){
			//db = dbIF.getDatabaseInstance();
			getLogs();
		};

		var setSendingInProgress = function(bool){
			sendingInProgress = bool;
		};
		
		var getLogs = function(){
			//get sessionHistory logs
			//var sessionHistoryLogs = getSessionHistoryLogs();
			
				//sendingInProgress = true;
				var sessionHistoryLogs;
				var mediaStatsLogs;

				var sHProm = getSessionHistoryLogs();
				var mSProm = getMediaStatsLogs();

				$q.all([
						sHProm.then(function gotSessionLogs(logs){
							sessionHistoryLogs = logs;
						}, 
							function didntGetLogs(text){
								console.log(text);
								eLF.logError(text);
						})
						,mSProm.then(function mediaLogsFetched(logs){
							mediaStatsLogs = logs
						}, 
							function didntFetchMediaLogs(text){
								console.log(text);
								eLF.logError(text);
						})
					]).then(function obtainedBothLogs(){
						reportStats(sessionHistoryLogs, mediaStatsLogs).then(function reportingSucceed(data){
							if(data.success){
								updateRecords(lGConfig.OSSessionHistory, lGConfig.sHSentToServerIndex);
								updateRecords(lGConfig.OSMediaStats, lGConfig.mSSentToServerIndex);
								//setSendingInProgress(false);
							}else{
								//try after some time
								eLF.logError(data.errors.join());
							}
						}, function reportingFailed(text){
							//try after sometime
							console.log(text);
							eLF.logError(text);
						})
					}, function couldntFetchLogs(){
						//try after some time
						console.log(text);
						eLF.logError('Couldnt Fetch Logs');
						//console.log('error while fetching data');
					});

				getErrorLogs().then(function obtainedErrorLogs(logs){
					reportLogs(logs).then(function reportedErrorLogs(data){
						if(data.success){
							updateRecords(lGConfig.OSErrorLogs, lGConfig.eLSentToServerIndex);
							//setSendingInProgress(false);
						}else{
							//try after some time
							eLF.logError(data.errors.join());
							tryReportingAgain();
						}
					}, function didntReportErrorLogs(text){
						console.log(text);
						eLF.logError(text);
						tryReportingAgain();
					});
				}, function didntObtainErrorLogs(text){
					console.log(text);
					eLF.logError(text);
				});
		};


		//sessionHistory logs
		var getSessionHistoryLogs = function(){
			var deferred = $q.defer();
			dbIF.getDatabaseInstance().then(function(db){
				var sessionHistoryLogs=[];
				var tx = db.transaction(lGConfig.OSSessionHistory, "readonly");
				var store = tx.objectStore(lGConfig.OSSessionHistory);
				var index = store.index(lGConfig.sHSentToServerIndex);

				var request = index.openCursor(IDBKeyRange.only("false"));
				request.onsuccess = function(){
					var cursor = request.result;
					var todayDate = dTCF.dateFormat(new Date());
					if(cursor){
						// Called for each matching record.
						// records except for today
						if(cursor.value.date != todayDate){
							sessionHistoryLogs.push(cursor.value);
						}
						cursor.continue();
					}else
					{
						// No more matching records.
						if(sessionHistoryLogs.length > 0){
							deferred.resolve(sessionHistoryLogs);
						}else{
							deferred.reject('no session logs records to send');
						}
					}					
				};

				request.onerror = function(){
					//Todo: log somewhere
					deferred.reject('fetch session logs error');
				};				
			});

			return deferred.promise;
		};

		//mediaStats logs
		var getMediaStatsLogs = function(){
			var deferred = $q.defer();
			dbIF.getDatabaseInstance().then(function(db){
				var mediaStatsLogs=[];

				var tx = db.transaction(lGConfig.OSMediaStats, "readonly");
				var store = tx.objectStore(lGConfig.OSMediaStats);
				var index = store.index(lGConfig.mSSentToServerIndex);

				var request = index.openCursor(IDBKeyRange.only("false"));
				request.onsuccess = function(){
					var cursor = request.result;
					var todayDate = dTCF.dateFormat(new Date())
					if (cursor){
						// Called for each matching record.
						if(cursor.value.date != todayDate){
							mediaStatsLogs.push(cursor.value);	
						}
						cursor.continue();
					} else {
						// No more matching records.
						if(mediaStatsLogs.length > 0){
							deferred.resolve(mediaStatsLogs);
						}else{
							deferred.reject('no media logs records to send');
						}
					}
				};
				request.onerror = function(){
					//Todo: log somewhere
					deferred.reject('fetch media logs error');
				};
			});

			return deferred.promise;
		};

		//error logs
		var getErrorLogs = function(){
			var deferred = $q.defer();
			dbIF.getDatabaseInstance().then(function(db){
				var errorLogs=[];

				var tx = db.transaction(lGConfig.OSErrorLogs, "readonly");
				var store = tx.objectStore(lGConfig.OSErrorLogs);
				var index = store.index(lGConfig.eLSentToServerIndex);

				var request = index.openCursor(IDBKeyRange.only("false"));
				request.onsuccess = function(){
					var cursor = request.result;
					var todayDate = dTCF.dateFormat(new Date())
					if (cursor){
						// Called for each matching record.
						if(cursor.value.date != todayDate){
							errorLogs.push(cursor.value);	
						}
						cursor.continue();
					} else {
						// No more matching records.
						if(errorLogs.length > 0){
							deferred.resolve(errorLogs);
						}else{
							deferred.reject('no error logs records to send');
						}
					}
				};
				request.onerror = function(){
					//Todo: log somewhere
					deferred.reject('error while fetching logs error');
				};
			});

			return deferred.promise;
		};

		//reporting to server
		var reportStats = function(session_time_list, media_item_stats_list){
			var deferred = $q.defer();
			var postData = {
				session_time_list : session_time_list
				,media_item_stats_list : media_item_stats_list
				,device_key : sDF.getDeviceKey()
			};
			$http({
                method : "POST",
                url : lGConfig.statsReportingUrl,
                data : postData,
            }).then(function mySucces(response) {
                deferred.resolve(response.data);
            }, function myError(response) {
                deferred.reject(response.Text)
            });
            return deferred.promise
		};

		var reportLogs= function(logs){
			/*
				Report all the error logs
			*/
			var deferred = $q.defer();
			var postData = {
				logs : logs
				,device_key : sDF.getDeviceKey()
			};
			$http({
                method : "POST",
                url : lGConfig.errLogsReportingUrl,
                data : postData,
            }).then(function mySucces(response) {
                deferred.resolve(response.data);
            }, function myError(response) {
                deferred.reject(response.Text)
            });
            return deferred.promise
		};


		//update sentToServer property
		var updateRecords = function(os, indexName){
			/*
				This function takes care of updating those records 
				which are sent to server.
			*/
			dbIF.getDatabaseInstance().then(function(db){
				var tx = db.transaction([os], 'readwrite');
				var store = tx.objectStore(os);
				var index = store.index(indexName);

				var request = index.openCursor(IDBKeyRange.only("false"));
				request.onsuccess = function(e){
					var cursor = e.target.result;
					var todayDate = dTCF.dateFormat(new Date());
					if(cursor){
						if(cursor.value.date != todayDate){
							var record = cursor.value;

							record[lGConfig.sent_to_server] = 'true';
							var res = cursor.update(record);
						}
						cursor.continue();
					}
				};
				dbIF.manageMemory();
			});
		};

		var tryReportingAgain = function(){
			$timeout(function(){
				getLogs();
			}, lGConfig.tryAgainDelay);
		}

		onLoad();
		//public methods

		return{

		};
	}]);


	//store screen activity
	lGApp.factory('loggingFactory', ['dBInitialise', 'lGConfig', '$q','dateTimeCustomisationsFactory','reportingFactory',
		function(dbIF, lGConfig, $q, dTCF, rf){
			var onLoad = function(){
			};

			//public methods
			var logData = function(osName, obj){
				/*
					args : OsName - Objectstore name in which the obj should be stored.
				*/
				if(dbIF.getDatabaseInstance()){
					var tx = dbIF.getDatabaseInstance().transaction(osName, "readwrite");
					var store = tx.objectStore(osName);
					store.put(obj);
				}
			}

			var startSession = function(logObj){
				dbIF.getDatabaseInstance().then(function(db){
					var tx = db.transaction(lGConfig.OSSessionHistory, "readwrite");
					var store = tx.objectStore(lGConfig.OSSessionHistory);
					logObj[lGConfig.sessionHistoryCol.sent_to_server] = 'false';
					logObj[lGConfig.sessionHistoryCol.session_start_time] = dTCF.getUTCDateTimeFormat();
					logObj[lGConfig.sessionHistoryCol.session_end_time] = dTCF.getUTCDateTimeFormat();
					logObj[lGConfig.sessionHistoryCol.date] = dTCF.dateFormat(new Date());
					store.add(logObj);
				});
			}

			var updateSessionLog = function(logObj){
				dbIF.getDatabaseInstance().then(function(db){
					var tx = db.transaction(lGConfig.OSSessionHistory, "readwrite");
					var store = tx.objectStore(lGConfig.OSSessionHistory);
					//get and update latest latest record;
					//var index = store.index(lGConfig.shSessionLogIndex);
					store.openCursor(null, 'prev').onsuccess = function(e){
						var cursor = e.target.result;
						if(cursor){
							var record = cursor.value;
							record[lGConfig.sessionHistoryCol.end_time] = dTCF.getUTCDateTimeFormat();
							var res = cursor.update(record);
						}
					}
				});
			}

			var logMediaStats = function(mediaData){
				dbIF.getDatabaseInstance().then(function(db){
					var record;
					var tx = db.transaction(lGConfig.OSMediaStats, "readonly");
					var store = tx.objectStore(lGConfig.OSMediaStats);
					var index = store.index(lGConfig.logMediaStatsIndex);
					var key_ranage = [dTCF.dateFormat(new Date()), mediaData[lGConfig.mediaStatsCol.playlist_id], mediaData[lGConfig.mediaStatsCol.content_id]];

					var request = index.get(key_ranage);

					request.onsuccess = function(event){
						record = event.target.result;
						tx = db.transaction(lGConfig.OSMediaStats, "readwrite");
						store = tx.objectStore(lGConfig.OSMediaStats);

						if(record){
							//update if already record exists
							record[lGConfig.mediaStatsCol.count] = record.count+1;
							record[lGConfig.mediaStatsCol.time_played] += mediaData.display_time;
							store.put(record);
						}else{
							//create record if record doesnt exist
							var logObj = {};
							logObj[lGConfig.mediaStatsCol.playlist_id] = mediaData.playlist_id;
							logObj[lGConfig.mediaStatsCol.content_id] = mediaData.content_id; 
							logObj[lGConfig.mediaStatsCol.count] = 1;
							logObj[lGConfig.mediaStatsCol.time_played] = mediaData.display_time;
							logObj[lGConfig.mediaStatsCol.date] = dTCF.dateFormat(new Date());
							logObj[lGConfig.mediaStatsCol.device_key] = mediaData.device_key;
							logObj[lGConfig.mediaStatsCol.sent_to_server] = 'false';
							store.add(logObj);
						}
					};
				});
			};

			onLoad();

			return{
				startSession : startSession
				,updateSessionLog : updateSessionLog
				,logMediaStats : logMediaStats
			}
	}]);

	//dateTimeCustom Functions
	lGApp.factory('dateTimeCustomisationsFactory', [function(){

        var prependZero = function(date_item){
            if(date_item < 10){
                return '0' + date_item.toString();
            }else{
                return date_item.toString();
            }
        };

        var dateTimeFormat = function(dateObj){
            var dd = prependZero(dateObj.getDate());
            var mm =  prependZero(dateObj.getMonth()+1);
            var yyyy = prependZero(dateObj.getFullYear());
            var hh = prependZero(dateObj.getHours());
            var mi = prependZero(dateObj.getMinutes());
            var se = prependZero(dateObj.getSeconds());

            return yyyy + mm + dd + hh + mi + se;
        };

        var dateFormat = function(dateObj){
            var dd = prependZero(dateObj.getDate());
            var mm =  prependZero(dateObj.getMonth()+1);
            var yyyy = prependZero(dateObj.getFullYear());

            return yyyy+'-'+mm+'-'+dd;
        };

        var getUTCDateTimeFormat = function(){
        	var now = new Date(); 
        	var utcDateTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
        	return dateTimeFormat(utcDateTime);
        };

        return{
        	dateTimeFormat : dateTimeFormat
        	,dateFormat : dateFormat
        	,getUTCDateTimeFormat : getUTCDateTimeFormat
        }
	}]);


	//error logs 
	lGApp.factory('errorLoggingFactory', ['lGConfig','dBInitialise','screenDetailsFactory',
		'dateTimeCustomisationsFactory',
	 function(lGConfig, dbIF, sDF, dTCF){
		/*
			This factory maintains a counter for no of timedout getScreens 
			requests. If no. of timedout requests cross a centain limit 
			that means the server is down/ network connection is gone. 

			Any of the above cases, would set the isRqstAllowed to false. 
			While fetching for media items, the request is sent only if the
			boolean is set to true.
		*/
		var httpRqstErrCounter=0;
		var isRqstAllowed = true;

		var setIsRqstAllowed = function(){
			if(httpRqstErrCounter >10){
				isRqstAllowed = false;
			}else{
				isRqstAllowed = true;
			}
		};

		var increaseCounter = function(){
			httpRqstErrCounter += 1;
			setIsRqstAllowed();
		};

		var resetCounter = function(){
			httpRqstErrCounter = 0;
			setIsRqstAllowed();
		};

		var logError = function(errObj){
			if(errObj && errObj != null){
				dbIF.getDatabaseInstance().then(function(db){
					var logObj ={};
					var tx = db.transaction(lGConfig.OSErrorLogs, "readwrite");
					var store = tx.objectStore(lGConfig.OSErrorLogs);
					logObj[lGConfig.errorLogsCol.sent_to_server] = 'false';
					logObj[lGConfig.errorLogsCol.date_time] = dTCF.getUTCDateTimeFormat();
					logObj[lGConfig.errorLogsCol.device_key] = sDF.getDeviceKey();
					if(typeof errObj == 'string'){
						logObj[lGConfig.errorLogsCol.error_text] = errObj;
					}else{
						if(errObj.statusCode){
							logObj[lGConfig.errorLogsCol.status_code] = errObj.statusCode;
						}
						if(errObj.text){
							logObj[lGConfig.errorLogsCol.error_text] = errObj.text;
						}
					}
					store.add(logObj);
				});
			}
		};

		return{
			increaseCounter : increaseCounter
			,resetCounter : resetCounter
			,isRqstAllowed : isRqstAllowed
			,logError : logError
		}


	}]);

})()