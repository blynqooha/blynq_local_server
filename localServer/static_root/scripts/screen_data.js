(function(){
	'use strict'

	/* 
		This module obtains the screen_key based on the type of the device the player is running.
		For samsung TV : from index page.

		The type of machine this code is running can be obtained from "navigator" object.
		Below are the properties of the navigator object
			--appVersion
			--platform 
			--userAgent
		By using above properties below code should be automatized
	*/

	var sDApp =  angular.module('sDApp', []).config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
    });

    sDApp.config(function($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    });

    sDApp.constant('sDconfig',{
    	deviceType : 'samsungTV'
    });

    sDApp.factory('screenDetailsFactory', ['sDconfig', function(config){

    	var getDeviceKey = function(){
    		var deviceKey;
    		switch (config.deviceType){
    			case 'samsungTV':
    				deviceKey = angular.element(document.getElementById('device_key')).val();
    				break;
    		};
    		return deviceKey;
    	};

    	return{
    		getDeviceKey : getDeviceKey
    	}

    }]);

})()