(function(){

	/*
		File Downloader Script
	*/

    var dLApp =  angular.module('dLApp', []).config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
    });

    dLApp.config(function($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    });


    dLApp.factory('downloaderFactory', [function(){

    	/* Works for mobile - http://stackoverflow.com/questions/21577230/phonegap-save-image-from-url-into-device-photo-gallery

	    	//private methods
			//step - 2 : get Write permission and Folder Creation
			function download(URL, Folder_Name, File_Name) {
				//step to request a file system 
			    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, fileSystemSuccess, fileSystemFail);

				function fileSystemSuccess(fileSystem) {
				    var download_link = encodeURI(URL);
				    ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL

				    var directoryEntry = fileSystem.root; // to get root path of directory
				    directoryEntry.getDirectory(Folder_Name, { create: true, exclusive: false }, onDirectorySuccess, onDirectoryFail); // creating folder in sdcard
				    var rootdir = fileSystem.root;
				    var fp = rootdir.fullPath; // Returns Fulpath of local directory
				    //var fp = rootdir.toURL(); --

				    fp = fp + "/" + Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give
				    // download function call
				    filetransfer(download_link, fp);
				}

				function onDirectorySuccess(parent) {
				    // Directory created successfuly
				}

				function onDirectoryFail(error) {
				    //Error while creating directory
				    alert("Unable to create new directory: " + error.code);
				}

				  function fileSystemFail(evt) {
				    //Unable to access file system
				    alert(evt.target.error.code);
				 }
			}


			//step-3 : transfer file into created folder
			function filetransfer(download_link, fp) {
				var fileTransfer = new FileTransfer();
				// File download function with URL and local path
				fileTransfer.download(download_link, fp,
	                    function (entry) {
	                        alert("download complete: " + entry.fullPath);
	                    },
	                 function (error) {
	                     //Download abort errors or download failed errors
	                     alert("download error source " + error.source);
	                     //alert("download error target " + error.target);
	                     //alert("upload error code" + error.code);
	                 }
	            );
			}


	    	//public methods
	    	//step - 1: check parameters mismatch and checking network connection if available call    download function
	    	var downloadFile = function(URL, Folder_Name, File_Name){
	    		//Parameters mismatch check
				if (URL == null && Folder_Name == null && File_Name == null) {
				    return;
				}
				else {
				    checking Internet connection availablity
				    var networkState = navigator.connection.type;
				    if (networkState == Connection.NONE) {
				        return;
				    } else {
				        download(URL, Folder_Name, File_Name); //If available download function call
				    }
				    download(URL, Folder_Name, File_Name); //If available download function call
				  }
	    	};

    	 */

	 	var downloadFile = function(url) {
		    var xhr = new XMLHttpRequest(); 
		    xhr.open('GET', url, true);
		    xhr.responseType = "blob";
		    // Set the responseType to arraybuffer. "blob" is an option too, rendering manual Blob creation unnecessary, but the support for "blob" is not widespread enough yet
		    //xhr.responseType = "arraybuffer";
		    xhr.onreadystatechange = function () { 
		        if (xhr.readyState == 4) {
		            if (success) success(xhr.response);
		        }
		    };
		    xhr.send(null);
		};

		/* using Html5 Apis */
		function success(xhrResponse){
			console.log(xhrResponse.type);
			console.log(xhrResponse.size);
			var path = 'ServerImage';
			var data =xhrResponse
			window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
			//window.storageInfo = window.storageInfo || window.webkitStorageInfo;

			// Request access to the file system
			var fileSystem = null         // DOMFileSystem instance
			  //, fsType = PERSISTENT       // PERSISTENT vs. TEMPORARY storage 
			  , fsSize = 10 * 1024 * 1024 // size (bytes) of needed space 
			  ;

			//window.storageInfo.requestQuota(fsType, fsSize, function(gb) {
			navigator.webkitPersistentStorage.requestQuota(fsSize,function(gb){
				console.log(gb);
			    window.requestFileSystem(fsType, gb, function(fs) {
			        fileSystem = fs;
			        saveFile()
			    }, errorHandler);
			}, errorHandler);

			var saveFile = function() {
		    	if (!fileSystem) return;

			    fileSystem.root.getFile(path, {create: true}, function(fileEntry) {
			        fileEntry.createWriter(function(writer) {
			            writer.write(data);
			        }, errorHandler);
			    }, errorHandler);
			};

			var errorHandler = function(e){
					var msg = '';

			  	switch (e.code) {
				    case FileError.QUOTA_EXCEEDED_ERR:
				      msg = 'QUOTA_EXCEEDED_ERR';
				      break;
				    case FileError.NOT_FOUND_ERR:
				      msg = 'NOT_FOUND_ERR';
				      break;
				    case FileError.SECURITY_ERR:
				      msg = 'SECURITY_ERR';
				      break;
				    case FileError.INVALID_MODIFICATION_ERR:
				      msg = 'INVALID_MODIFICATION_ERR';
				      break;
				    case FileError.INVALID_STATE_ERR:
				      msg = 'INVALID_STATE_ERR';
				      break;
				    default:
				      msg = 'Unknown Error';
				      break;
				}
				console.log(msg);
			};



		};

		var readFile = function(path, success) {
		    fileSystem.root.getFile(path, {}, function(fileEntry) {
		        fileEntry.file(function(file) {
		            var reader = new FileReader();

		            reader.onloadend = function(e) {
		                if (success) success(this.result);
		            };

		            reader.readAsText(file);
		        }, errorHandler);
		    }, errorHandler);
		};

		//using Smaf
		// function success(xhrResponse){
		// 	var	blob = new Blob([xhr.response], {type: "image/png"});
		// 	var fileReader = new FileReader();

		// 	//Once fileReader loads the data, it saves it to Smaf.storage.
		// 	fileReader.onload=function(evnt){
		// 		try {
  //                   Smaf.storage().setItem("image", evnt.target.result).then(function(){
  //                   	//afterstoring functions.
  //                   });
  //               }
  //               catch (e) {
  //                   console.log("Storage failed: " + e);
  //               }
		// 	}

		// 	fileReader.readAsDataURL(blob);
		// };

    	return{
    		downloadFile : downloadFile
    	}
    }]);

})();





