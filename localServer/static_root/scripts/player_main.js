(function () {
    'use strict'

    var mainApp =  angular.module('mainApp', ['sDApp', 'lGApp', 'ngPDFViewer']).config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
    });

    mainApp.config(function($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    });

    mainApp.constant('config', {
        //will call local server - "/api/player/getScreenData"
        //url : "http://www.blynq.in/api/player/getScreenData"
        url : "/api/player/getScreenData"
        ,pollInterval : 60 * 1000
        ,layoutTemplateUrl : '/_layout.html'
        ,paneTemplateUrl : '/_pane.html'
        ,mediaPlayerTemplateUrl : '/_media_player.html'
        ,isLoggingActive : true
        ,isErrorLoggingActive : true
        ,defaultImageUrl : 'static/img/default_img.jpg'
        ,loadingImageUrl : 'static/img/default_img.jpg'
        ,onErrorIntervalDuration : 10*1000
        ,rqstTimoutDuration : 10 * 1000
    });

    mainApp.factory('DataAccessFactory',['$http','$q','config','screenDetailsFactory',
        'errorLoggingFactory',
     function ($http, $q, config, sDF, eLF) {

        var last_received = new Date("2016-01-01");

        var prependZero = function(date_item){
            if(date_item < 10){
                return '0' + date_item.toString();
            }else{
                return date_item.toString();
            }
        };

        var customDateFormat = function(last_received){
            var dd = prependZero(last_received.getDate());
            var mm =  prependZero(last_received.getMonth()+1);
            var yyyy = prependZero(last_received.getFullYear());
            var hh = prependZero(last_received.getHours());
            var mi = prependZero(last_received.getMinutes());
            var se = prependZero(last_received.getSeconds());

            return dd + mm + yyyy + hh + mi + se;
        };

        var pollServer = function() {
            var postData = {
                device_key : sDF.getDeviceKey(),
                last_received : customDateFormat(last_received)
            };
            var deferred = $q.defer();
            $http({
                method : "POST",
                url : config.url,
                data : postData, 
                config : {timeout : config.rqstTimoutDuration}
            }).then(function mySucces(response) {
                 var now = new Date(); 
                //Below line has time of UTC, and zone as IST. (Since we are using Zone here, it can be ignored).
                last_received = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
                deferred.resolve(response.data);
                if(config.isErrorLoggingActive){
                    eLF.resetCounter();
                }
            }, function myError(response) {
                //console.log(response.statusText);
                deferred.reject(response.Text);
                if(response.status == 0 || response.status == -1){
                    if(config.isErrorLoggingActive){
                        eLF.increaseCounter();
                    }
                }
            });
            return deferred.promise
        };

        return{
            pollServer : pollServer
        } 
    }]);

    mainApp.factory('scheduleInterpretterFactory',['$q','DataAccessFactory','$interval','config',
        'loggingFactory','lGConfig','screenDetailsFactory',
     function($q, dAF, $interval, config, lF, lGConfig, sDF) {
        //declarations
        var obseverCallback;
        var intervalPromise;
        var nowPlayingSchedules=[];
        var masterCopyCampaigns;

        //private methods
        var onLoad = function () { //This will start off polling
            getSchedulesFromServer();
            if(config.isLoggingActive){
                startSessionLog();
            }
        };

        intervalPromise = $interval(function(){
            getSchedulesFromServer();
            if(config.isLoggingActive){
                updateSessionLog();
            }
        },config.pollInterval);


        var getSchedulesFromServer = function () {
            var deferred = $q.defer();
            dAF.pollServer().then(function pollServerSuccess(pollResponse) {
                if(typeof masterCopyCampaigns == 'undefined' || pollResponse.is_modified){
                    updateMasterCopyCampaign(pollResponse.campaigns);
                }else{
                    checkNowPlayingSchedulesChanges();
                }
            }, function pollServerFail(argument) {
                
            });
            return deferred.promise
        };
        
        var updateMasterCopyCampaign = function (data) {
        	if(data){
        		masterCopyCampaigns = data;
            	checkNowPlayingSchedulesChanges();
        	}
        };

        var checkNowPlayingSchedulesChanges = function () {
            var currentSchedules =  getCurrentSchedules();
            var isCurrentSchedulesupdated = !angular.equals(nowPlayingSchedules, currentSchedules);

            if(isCurrentSchedulesupdated){
                updateNowPlayingSchedules(currentSchedules);
            }
        };

        var getCurrentSchedules = function (argument) {
            var totalCampaigns = masterCopyCampaigns.length;
            var currentTime = new Date();
            var startTime;
            var endTime;
            var currentSchedules=[];

            for(var i=0; i<totalCampaigns; i++){
                startTime = new Date(masterCopyCampaigns[i].start_time);
                endTime = new Date(masterCopyCampaigns[i].end_time);
                if((currentTime < endTime)){
                    if((currentTime > startTime)){
                        currentSchedules.push(cookSchedule( angular.copy(masterCopyCampaigns[i])));
                    }
                    else{
                        break;
                    }
                }
                
            }

            return currentSchedules
        };

        var cookSchedule = function (schedule) {
            //This function clubs all the playlistItems in a schedule and maintains single queue
            // as schedule.all_playlist_items = [];
            var allPlaylistItems = [];
            delete schedule['last_updated_time'];
            for(var i=0; i< schedule.playlists.length; i++){
                var playlistLen = schedule.playlists[i].playlist_items.length;
                var playlist_id = schedule.playlists[i].playlist_id;
                for(var j=0; j<playlistLen; j++){
                    schedule.playlists[i].playlist_items[j]['playlist_id'] = playlist_id;
                }
                allPlaylistItems =  allPlaylistItems.concat(schedule.playlists[i].playlist_items);
            }
            schedule.all_playlist_items = allPlaylistItems;

            return schedule
        };

        var updateNowPlayingSchedules = function(currentSchedules){
            //same number -- layout change/objects change
            //check for each pane
            if(nowPlayingSchedules.length != currentSchedules.length){
                nowPlayingSchedules = currentSchedules;
            }
            else
            {
                for(var i=0; i < currentSchedules.length; i++)
                {
                    //find currentSchedules[i] pane obj in nowPlayingSchedules
                    var indexOfPaneInNowPlaying = getPaneInd(currentSchedules[i].pane.layout_pane_id);
                    if(indexOfPaneInNowPlaying != -1){
                        if(!angular.equals(nowPlayingSchedules[indexOfPaneInNowPlaying], currentSchedules[i]))
                        {
                            nowPlayingSchedules[indexOfPaneInNowPlaying] = currentSchedules[i];
                        }
                    }else{
                        nowPlayingSchedules.push(currentSchedules[i]);
                    }
                }
            }
        };       
        
        var getPaneInd = function(layout_pane_id){
            //returns an index of the pane obj with layout_pane_id in the nowPlayingSchedules
            var ind = -1;
            for(var i=0; i<nowPlayingSchedules.length; i++){
                if(nowPlayingSchedules[i].pane.layout_pane_id == layout_pane_id){
                    ind = i;
                    return ind
                }
            }
            return ind
        };

        //related to logs
        var startSessionLog = function(){
            var logObj = {};
            logObj.device_key = sDF.getDeviceKey();
            lF.startSession(logObj);
        };

        var updateSessionLog = function(){
            var logObj = {};
            logObj.device_key = sDF.getDeviceKey();
            lF.updateSessionLog(logObj);
        };


        // var logSession = function(){
        //     var record={};
        //     record.activeOn = Date();
        //     record.device_key = sDF.getDeviceKey();
        //     record[lGConfig.sentToServer] = 'false';
        //     lF.logData(lGConfig.OSSessionHistory, record);
        // }


        //public methods
        var getNowPlayingSchedules = function(){
            //This function is the connector to the controller.
            //Whenever this updates, the value in the controller updates.
            return nowPlayingSchedules
        };

        onLoad();


        return{
            getNowPlayingSchedules : getNowPlayingSchedules
        }
    }]);

    mainApp.controller('mainCtrl', ['$scope','$interval','scheduleInterpretterFactory','config',
     function($scope, $interval, sIF, config) {
        var onLoad = function () {
            $scope.getNowPlayingSchedules = sIF.getNowPlayingSchedules;
        };

        onLoad();
    }]);

    mainApp.directive('layout',['config', function (config) {
        /* This directive maintains the same scope of controller.
            
        */
        return{
            restrict : 'E'
            ,templateUrl : config.layoutTemplateUrl
        }
    }]);

    mainApp.directive('pane', ['config', function (config) {
        /*
            Pane directive is reused and hence maintains isolate scope.
        */
        return{
            restrict : 'E'
            ,templateUrl : config.paneTemplateUrl
            ,scope : {
                schedulePane : '=schedulePane'
            }
            ,link : function postLink($scope, elem, attr) {
                //console.log($scope.schedulePane)
                
            }
        }
    }]);

    mainApp.directive('mediaPlayer', ['$timeout','config','loggingFactory','lGConfig','screenDetailsFactory',
    	'$interval','$q','$http','errorLoggingFactory',
     function ($timeout, config, lF, lGConfig, sDF, $interval, $q, $http, eLF) {
        /* Media player directive gets the list of all the items
            to be shown on the pane. It detects the the type of media
            and detects appropriate type to show the picture for the 
            duration given.
            maintains the same scope as parent 'pane'
        */
        return{
            restrict : 'E'
            ,templateUrl : config.mediaPlayerTemplateUrl
            ,link : function postLink($scope, elem, attr) {
                var timeoutPromise;
                var totalPlayableItems;
                var onErrorCheckProm;
                var onLoad = function (argument) {
                	$scope.defaultImageUrl = config.defaultImageUrl;
                	$scope.loadingImageUrl = config.loadingImageUrl;
                    totalPlayableItems = $scope.schedulePane.all_playlist_items.length;
                    if(totalPlayableItems >0){
                     	startNewPlaylistItem();
                    }
                    else{
                    	$scope.isMediaType.default= true;
                    }
                };

                var startNewPlaylistItem = function(){
                	//clear any set-up of previous playlistItem
                	stopOnErrorCheckProm();
                	cancelTimeoutProm();
                    setIsLoading(false);

                    //set up and actions for new item
                    updatePlayingItemIndexAndDuration();
                    setMediaType();
                    setTimeout();
                    if(config.isLoggingActive){
                        updateLog();
                    }
                };

                var setTimeout = function () {
                    // body...
                    timeoutPromise = $timeout(startNewPlaylistItem, $scope.playingItemDuration);
                };

                var updatePlayingItemIndexAndDuration = function () {
                    // body...
                    if(angular.isDefined($scope.playingItemIndex)){
	                    if(($scope.playingItemIndex+1) < totalPlayableItems)
	                    {
	                        $scope.playingItemIndex += 1;
	                    }
	                    else{
	                        $scope.playingItemIndex =0;
	                    }                   	
                    }else{
                    	$scope.playingItemIndex =0;
                    }
                    $scope.playingItemDuration =$scope.schedulePane.all_playlist_items[$scope.playingItemIndex].display_time *1000;
                };

                var updateLog = function(){
                    /* 
                        log new media play time in mediaStats
                        schedule_id, layout_pane_id, playlist_item_id, start-dateTime, 

                    */
                    if( typeof $scope.schedulePane != 'undefined' && typeof $scope.schedulePane.all_playlist_items != 'undefined' &&  ($scope.schedulePane.all_playlist_items.length > 0)){
                        var logObj = {};
                        // logObj.schedule_id = $scope.schedulePane.schedule_id;
                        // logObj.layout_pane_id = $scope.schedulePane.pane.layout_pane_id;
                        // logObj.playlist_item_id = $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].playlist_item_id;
                        // logObj.played_on = Date();
                        // logObj.sentToServer = 'false'
                        logObj[lGConfig.mediaStatsCol.playlist_id] = $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].playlist_id;
                        logObj[lGConfig.mediaStatsCol.content_id] = $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].content_id;
                        logObj.display_time = $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].display_time;
                        logObj.device_key = sDF.getDeviceKey();

                        lF.logMediaStats(logObj);
                    }
                };

                $scope.isMediaType = {
                    image : false
                    ,video : false
                    ,audio : false
                    ,pdf : false
                    ,youtube : false
                    ,rssText : false
                    ,iframe : false
                    ,default : false
                };

                var setMediaType = function(){
                    /*
                        sets what mime type the current playing item is. If it can't find any, it defaults to 
                        default image. 
                    */
                    angular.forEach($scope.isMediaType, function(value, key){
                        $scope.isMediaType[key] = false;
                    });
                    switch(true){
                        case $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].content_type.indexOf('image')>-1:
                            $scope.isMediaType.image = true;
                            break;
                        case $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].content_type.indexOf('pdf')>-1:
                            $scope.isMediaType.pdf = true;
                            break;
                        case $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].content_type.indexOf('video')>-1:
                            $scope.isMediaType.video = true;
                            break;
                        case $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].content_type.indexOf('audio')>-1:
                            $scope.isMediaType.audio = true;
                            break;
                        case $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].content_type.indexOf('youtube')>-1:
                            $scope.isMediaType.youtube = true;
                            break;
                        case $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].content_type.indexOf('widget/rss/text')>-1:
                        	$scope.isMediaType.rssText = true;
                        	setUpRssTextMedia();
                        	break;
                        case $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].content_type.indexOf('url/web')>-1:
                            $scope.isMediaType.iframe = true;
                            break;
                        default:
                            $scope.isMediaType.default = true;  
                    }
                };

                var setIsLoading = function(bool){
                	/*
                		This bool reflects if file doenst exist in server and need to show 
                		default image
                	*/
                	$scope.isLoading = bool;
                };

                var stopOnErrorCheckProm = function(){
                	/*
						This stops the onError interval Promise (if any),  that was intiated to 
						obtain the file as soon as it is downloaded/received at the player side.
                	*/
                 	if(angular.isDefined(onErrorCheckProm)){
                		$interval.cancel(onErrorCheckProm);
                		onErrorCheckProm = undefined;
                	}               	
                };

                var cancelTimeoutProm = function(){
                	if(angular.isDefined(timeoutPromise)){
                		$timeout.cancel(timeoutPromise);
                	}
                };

                $scope.initiateOnErrorFunc = function(){
                    setIsLoading(true);
                	var checkIfSourceExists = function(){
    					checkServerForSource().then(function exists(){
    						//abort the interval function and switch off default and switchon the actual
    						stopOnErrorCheckProm();
    						setIsLoading(false);
    					}, function doesntExist(){
    						//switch the default image and keep sending requests
    						setIsLoading(true);
    					});
					};
    				onErrorCheckProm = $interval( function(){
                            checkIfSourceExists();
    				 }, config.onErrorIntervalDuration);
                };

                var checkServerForSource = function(){
				    /*
                        Checks only if the the boolean is
                    */
                    var deferred = $q.defer();
                    if(eLF.isRqstAllowed){
                        $http({
                            method : "GET",
                            url : $scope.schedulePane.all_playlist_items[$scope.playingItemIndex].url,
                        }).then(function mySucces(response) {
                            deferred.resolve();
                        }, function myError(response) {
                            deferred.reject();
                        });
                    }else{
                        deferred.reject();
                    }
                    return deferred.promise;
			    };

			    var setUpRssTextMedia = function(){
			    	//set font-size according to the div height
			    	var $div = $('.media-player.'+ $scope.schedulePane.pane.layout_pane_id+ ' .rss-text')
			    	var divHeight = $div.height();
			    	$div.css({
						'font-size': (divHeight/2) + 'px',
						'line-height': divHeight + 'px'
					});
			    };


			    $scope.$watch('schedulePane', function(){
			    	//on new schedulePane
			    	//onLoad();
			    });

		        elem.on('$destroy', function() {
      				stopOnErrorCheckProm();
      				$timeout.cancel(timeoutPromise);
    			});

                onLoad();
            }
        }
    }]);

    mainApp.directive('muteDir', [ function(){
        /*
            MuteDir adds the attr 'muted' to elem based on the 
            mute_audio boolean set by the users in a schedule.
        */
        return{
            restrict : 'A'
            ,link : function($scope, elem, attr){
                if(attr['muteDir'] == 'true')
                {
                    elem.attr('muted', '');
                }
                else{
                    elem.removeAttr('muted');
                }
            }
        }
    }]);

    mainApp.directive('onErrorDrtv', [function(){
        /*
            This module binds the onerror to the function 
            written in mediaPlayer directive, which will query
            on intervals to check. 

            This maintains the same scope as of media player.
        */
        return{
            restrict : 'A'
            ,link : function($scope, elem){
                elem.bind('error', function(e){
                    $scope.initiateOnErrorFunc();
                 });
            }
        }
    }]);

    mainApp.directive('pdfSection', ['PDFViewerService','$timeout', function(pdf, $timeout){
        /*
            This directive has the same scope as its parents. 
            This calculates a time period for each page, based on total pages and total time allotted and 
            flips every page after the calculated period. 
        */

            return{
                restrict : 'E'
                ,link   : function($scope, elem, attr){

                    var pdfTotalDuration;
                    var pdfPageDuration;

                    var onLoad = function(){
                        $scope.viewer = pdf.Instance("viewer");
                        pdfTotalDuration = $scope.playingItemDuration;
                    };
                    onLoad();

                    $scope.nextPage = function() {
                        $scope.viewer.nextPage();
                    };

                    $scope.pageLoaded = function(curPage, totalPages) {
                        pdfPageDuration = pdfTotalDuration/totalPages;
                        $timeout(function(){
                            $scope.nextPage();
                        }, pdfPageDuration);
                        $scope.currentPage = curPage;
                        $scope.totalPages = totalPages;
                    };
                }
            }
    }]);

    mainApp.filter('trusted', ['$sce', function ($sce) {
        return function(url) {
            return $sce.trustAsResourceUrl(url);
        };
    }]);    

    mainApp.filter('youtube', ['youtubeFactory', function(yF){
        /*
            This filter extracts the required video Id and playlist Id 
            and builds a custom Url which is upported to play
         */
        return function(url){
            var controls = '?controls=0';
            var autoplay = '&autoplay=1';
            var loop = '&loop=1';
            var stopAnnotations = '&iv_load_policy=3';
            var playlistUrlStr = '?listType=playlist&list=';
            var addAdditionalParamsToUrl = function(){
                customUrl = customUrl + controls + autoplay + loop + stopAnnotations;
            };

            var customUrl = 'https://www.youtube.com/embed/';

            //if video Id exists, then construct url with video Id, otherwise with playlistId
            if(yF.getVideoId(url)){
                customUrl = customUrl + yF.getVideoId(url);
                addAdditionalParamsToUrl();
                return customUrl
            }else{
                customUrl = customUrl +  playlistUrlStr + yf.getPlaylistId(url);
                return customUrl
            }
        }
    }]);

    mainApp.factory('youtubeFactory', [function(){

        //returns the video Id of an Url
        var getVideoId = function(url){
            var regex = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            return url.match(regex)[7]
        };

        var getPlaylistId = function(url){
            var regExp = /^.*(youtu.be\/|list=)([^#\&\?]*).*/;
            var match = url.match(regExp);
            if (match && match[2]){
            return match[2];
            }
        };

        return{
            getVideoId : getVideoId
            ,getPlaylistId : getPlaylistId
        };
    }]);     

})();