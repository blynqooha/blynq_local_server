import os

string_to_find="branding"
replacement = "jaydev"
dir_path_to_scan="/Users/Jaydev/Desktop/BlYnQ/test/test/localServer/config_files"

for dname, dirs, files in os.walk(dir_path_to_scan):
    for fname in files:
        fpath = os.path.join(dname, fname)
        with open(fpath) as f:
            s = f.read()
        s = s.replace(string_to_find, replacement)
        with open(fpath, "w") as f:
            f.write(s)