RELATIVE_DIRECTORIES = {
    "blynq_dir": "blynq/",
    "media_dir": "blynq/media/",
    "temp_dir": "blynq/temp/",                  # We are using this directory for partial downloaded files
    "others_dir": "blynq/others/",
    "screens_data_dir": "blynq/screens_data/",
    "player_logs_dir": "blynq/player_logs/",
    "default_dir": "blynq/default/",
}

BASE_URL = 'http://www.blynq.in/api/player/'
SCHEDULES_URL = BASE_URL + 'getScreenData'
ACTIVATION_URL = BASE_URL + 'activationKeyValid'
STATS_URL = BASE_URL + 'mediaStats'

LOCAL_SERVER_URL = 'http://192.168.1.12:8080/blynq/media/'        # TODO - ensure mapping and abstract IP

COMPARATOR_STR = 'blynq.in/'

POLL_INTERVAL = 60  # in seconds
MAXIMUM_DISK_USAGE = 0.80

# String Tags
CONTENT_TYPE_TAG = 'content_type'
URL_TAG = 'url'
ORIGINAL_URL_TAG = 'original_url'

# Runtime variables - subject to change during program execution
# ['f00faa59270d5f7', '01012016000000'], ['452e194447f14eb6', '01012016000000']
DEVICE_INFO_LIST = []
DOWNLOAD_URL_OBJS = []
