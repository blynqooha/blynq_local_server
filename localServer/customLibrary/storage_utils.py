import os

from customLibrary import settings
from customLibrary.settings import RELATIVE_DIRECTORIES
from localServer.settings import debugFileLog, BASE_DIR


def is_local_file_necessary(content_type):
    return not (content_type.startswith("url/web") or content_type.startswith('widget'))


def get_device_info_list():
    debugFileLog.info('inside get_device_info_list')
    return settings.DEVICE_INFO_LIST


def device_key_exists(device_key):
    debugFileLog.info('inside device_key_exists %s' % device_key)
    device_info_list = get_device_info_list()
    found = False
    for info in device_info_list:
        if info[0] == device_key:
            found = True
            break
    debugFileLog.info('device_key found %s' % found)
    return found


def get_last_received(device_key):
    debugFileLog.info('inside get_last_received %s' % device_key)
    device_info_list = get_device_info_list()
    for info in device_info_list:
        if info[0] == device_key:
            debugFileLog.info('last_received ')
            return info[1]
    default_last_received = '01012016000000'
    write_device_info(device_key, last_received=default_last_received)
    return default_last_received


def write_device_info(device_key, last_received):
    device_info_list = get_device_info_list()
    found = False
    for info in device_info_list:
        if info[0] == device_key:
            info[1] = last_received
            found = True
    if not found:
        device_info_list.append([device_key, last_received])


def return_old_files(folder):
    """
    :param folder:
    :return: recursively the list of files
    """
    matches = []
    for root, dirnames, filenames in os.walk(folder):
        for filename in filenames:
            matches.append(os.path.join(root, filename))
    return sorted(matches, key=os.path.getatime)


# TODO: inconsistency between urls and local files if deleted
def delete_old_files():
    blynq_directory = os.path.join(BASE_DIR, settings.RELATIVE_DIRECTORIES["media_dir"])
    old_files = return_old_files(blynq_directory)
    debugFileLog.info('old files %s ' % old_files)
    for the_file in old_files:
        try:
            os.remove(the_file)
        except Exception as e:
            print 'Exception while deleting old file %s' % the_file
            print str(e)
        if disk_usage(".") <= settings.MAXIMUM_DISK_USAGE:
            break
    debugFileLog.info('delete_old_files completed successfully')


def check_disk_space():
    debugFileLog.info('inside check_disk_space')
    try:
        if disk_usage(".") > settings.MAXIMUM_DISK_USAGE:
            from localServer import delete_old_files
            delete_old_files()
    except Exception as e:
        debugFileLog.exception(e)


def disk_usage(path):
    """Computes disk usage statistics about the given path.
    which are the amount of total and used  in bytes.
    Returns percentage free memory
    """
    st = os.statvfs(path)
    # free = st.f_bavail * st.f_frsize
    total = st.f_blocks * st.f_frsize
    used = (st.f_blocks - st.f_bfree) * st.f_frsize
    return used/total


def file_path_from_directory_key(directory_key, filename):
    relative_dir_path = RELATIVE_DIRECTORIES[directory_key]
    absolute_dir_path = os.path.join(BASE_DIR, relative_dir_path)
    file_path = os.path.join(absolute_dir_path, filename)
    return file_path