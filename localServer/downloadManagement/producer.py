import json, requests, urllib
import os, shutil
from uuid import getnode

from localServer import debugFileLog

"""
if url of a file on the cloud server is http://www.blynq.in/media/usercontent/1/sachin.jpg
and the ip address of the local server is http://10.10.10.10
then, the url of the file on the local server would be
http://10.10.10.10/blynq/www.blynq.in/media/usercontent/1/sachin.jpg ( local_ip + '/blynq/' + cloud_url )
i.e, replace http:/ in the cloud url with 'blynq' and prepend the local server url
"""

# Make sure download directory points to that of the http server root + blynq
download_directory = '/Users/nipun/PycharmProjects/localServer/blynq'

filename = 'downloaded_files.txt'

url = 'http://www.blynq.in/api/schedule/getContentUrlsLocal'


# local_url = 'http://127.0.0.1:8000/api/schedule/getContentUrlsLocal'
# url = local_url


def device_mac_address():
    return str(getnode())


def string_to_dict(str):
    return json.loads(str)


def remove_http(url):
    return url.rsplit('//')[-1]  # remove http://


def download_file(url):
    file_url = urllib.unquote(url)
    full_file_path = remove_http(file_url)

    # Combine the name and the downloads directory to get the local filename
    filename = os.path.join(download_directory, full_file_path) #TODO - filename change

    # create the directory structure if not exists
    directory = os.path.dirname(filename)
    if not os.path.exists(directory):
        os.makedirs(directory)

    if not os.path.isfile(filename):
        try:
            urllib.urlretrieve(url, filename)
            return True
        except Exception as e:
            debugFileLog.exception('Received exception while downloading file %s' % filename)
            debugFileLog.exception(e)
            return False
    else:
        print '%s already exists' % url
        return True


def fetch_urls():
    global url
    try:
        response = requests.post(url=url, json={"unique_key": device_mac_address()})
        if response.status_code == 200:
            json_content = string_to_dict(response.content)
            if json_content.get('success'):
                urls_list = json_content.get('urls')
                return urls_list
            else:
                print 'Received success False while fetching content urls to local server'
        else:
            print "Received status code %d, reason %s" % (response.status_code, response.reason)
    except Exception as e:
        print 'Received exception while fetching list of urls \n %s \n check internet connection' % str(e)
    return []


def write_to_file(filename, data_list, mode='a'):
    with open(filename, mode) as the_file:
        the_file.writelines(data_list)
        the_file.close()


def full_cleanup():
    shutil.rmtree(download_directory)
    os.makedirs(download_directory)


def partial_cleanup(downloaded_files):
    """
    :param: downloaded_files indicates the list of files which shouldn't be deleted
    :return: will only delete files in the downloads directory which are not in the
    """
    for root, directories, filenames in os.walk(download_directory):
        removed_filenames = []
        for filename in filenames:
            file_path = os.path.join(root, filename)
            if not file_path in downloaded_files:
                try:
                    os.remove(file_path)
                    print 'Deleted file %s' % file_path
                except Exception as e:
                    print 'Not able to delete file %s' % file_path
                    print 'Exception is %s' % str(e)
                removed_filenames.append(filename)

        if set(removed_filenames) == set(filenames) and not directories:
            # Both files and directories are empty, so remove directory
            try:
                os.rmdir(root)
                print 'Deleted directory %s' % root
            except Exception as e:
                print 'Not able to delete empty directory %s' % root
                print 'Exception is %s' % str(e)


def start_app():
    urls_list = fetch_urls()
    print 'Downloading...'
    completed_list = []
    incomplete_list = []
    for url in urls_list:
        success = download_file(url)
        if not success:
            print 'Failed to download url %s ' % url
            for i in range(2):
                print 'Retrying... time %d' % (i + 2)
                success = download_file(url)
                if success:
                    break
        if success:
            completed_list.append(url)
            print 'Downloaded %s' % url
        else:
            incomplete_list.append(url)
    write_to_file(filename, completed_list)
    print 'Download Complete'
    return completed_list


if __name__ == "__main__":
    downloaded_files = start_app()
    partial_cleanup(downloaded_files)
