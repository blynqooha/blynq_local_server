import json
import os
import urllib
import concurrent.futures
import functools
import time
import wget

from localServer.settings import BASE_DIR, debugFileLog
from customLibrary.storage_utils import is_local_file_necessary, get_device_info_list, check_disk_space, \
    file_path_from_directory_key
from customLibrary import settings as properties


media_download_manager = concurrent.futures.ThreadPoolExecutor(max_workers=2)


def timeit(func):
    @functools.wraps(func)
    def newfunc(*args, **kwargs):
        startTime = time.time()
        response = func(*args, **kwargs)
        elapsedTime = time.time() - startTime
        debugFileLog.info('function [{}] finished in {} ms'.format(func.__name__, int(elapsedTime * 1000)))
        return response
    return newfunc


def file_already_downloaded(local_url):
    debugFileLog.info('inside file_already_downloaded with local_url %s' % local_url)
    file_path = absolute_path_from_local_url(local_url)
    filename = os.path.basename(file_path)
    temp_file_path = os.path.join(properties.RELATIVE_DIRECTORIES['temp_dir'], filename)
    if os.path.exists(file_path) or os.path.exists(temp_file_path):
        return True
    else:
        return False


def absolute_path_from_local_url(local_url):
    local_url = urllib.unquote(local_url)
    pos = local_url.find(properties.LOCAL_SERVER_URL)
    if pos != -1:
        relative_path = local_url.replace(properties.LOCAL_SERVER_URL, '', 1)
        absolute_file_path = os.path.join(BASE_DIR, properties.RELATIVE_DIRECTORIES['media_dir'], relative_path)
        return absolute_file_path
    else:
        debugFileLog.exception('BASE_URL %s not found in local_url %s' % (properties.LOCAL_SERVER_URL, local_url))
        return ''


@timeit
def download_media_job(url_obj):
    try:
        remote_url = url_obj.get('remote_url')
        local_url = url_obj.get('local_url')
        if not (remote_url or local_url):
            debugFileLog.error('remote_url %s or local_url %s does not exist' % (remote_url, local_url))
            return True
        if file_already_downloaded(local_url):
            debugFileLog.info('%s is already downloaded' % local_url)
            return True

        debugFileLog.info('Downloading from url %s ' %(remote_url))
        temp_dir = file_path_from_directory_key('temp_dir', '')
        dest_file_path = absolute_path_from_local_url(local_url)
        filename = os.path.basename(dest_file_path)
        temp_file_path = os.path.join(temp_dir, filename)
        if not os.path.exists(temp_dir):
            os.makedirs(temp_dir)

        # create the directory structure if not exists
        dest_file_dir = os.path.dirname(dest_file_path)
        if not os.path.exists(dest_file_dir):
            os.makedirs(dest_file_dir)

        try:
            check_disk_space()
            # urllib.urlretrieve(remote_url, temp_file_path)
            wget.download(remote_url, temp_file_path)
            # Move file from temp folder to destination folder and delete the file in the temp directory
            os.rename(temp_file_path, dest_file_path)
            # os.remove(temp_file_path)
            return True
        except Exception as e:
            debugFileLog.exception('Received exception while downloading file %s from url %s' % (dest_file_path, remote_url))
            debugFileLog.exception(e)
            properties.DOWNLOAD_URL_OBJS.remove(url_obj)
            try:
                os.remove(temp_file_path)
            except:
                pass
            return False
    except Exception as e:
        debugFileLog.exception(e)


def extract_downloadable_urls(json_data):
    urls = []
    for key in json_data.keys():
        if key == properties.ORIGINAL_URL_TAG and is_local_file_necessary(json_data[properties.CONTENT_TYPE_TAG]):
            urls.append({'remote_url': json_data[key], 'local_url': json_data[properties.URL_TAG]})
        elif type(json_data[key]) is dict:
            urls += extract_downloadable_urls(json_data[key])
        elif type(json_data[key]) is list:
            for element in json_data[key]:
                urls += (extract_downloadable_urls(element))
    return urls


def filter_unique_url_objs(url_objs):
    # Filter unique url objects
    url_objs = {obj['local_url']:obj for obj in url_objs}.values()
    if not properties.DOWNLOAD_URL_OBJS:
        return url_objs
    # Only add the urls if they don't already exist in the media_download_manager map
    new_url_objs = []
    for obj in url_objs:
        if obj not in properties.DOWNLOAD_URL_OBJS:
            new_url_objs.append(obj)
    return new_url_objs


def download_media_for_screen(device_key):
    file_path = os.path.join(BASE_DIR, properties.RELATIVE_DIRECTORIES["screens_data_dir"], device_key)
    with open(file_path) as f:
        json_str = f.read().replace('\n', '')
    json_data = json.loads(json_str)
    trigger_necessary_downloads(json_data)


def trigger_necessary_downloads(json_data):
    debugFileLog.info('inside trigger_necessary_downloads')
    try:
        url_objs = extract_downloadable_urls(json_data)
        url_objs = filter_unique_url_objs(url_objs)
        debugFileLog.info("Downloadable urls: " + str(url_objs))
        media_download_manager.map(download_media_job, url_objs)
        properties.DOWNLOAD_URL_OBJS = properties.DOWNLOAD_URL_OBJS + url_objs
    except Exception as e:
        debugFileLog.exception(e)
        debugFileLog.info('json_data is %s' % json_data)


def download_all_screens_media():
    try:
        device_info_list = get_device_info_list()
        for screen_info in device_info_list:
            device_key = screen_info[0]
            download_media_for_screen(device_key)
    except Exception as e:
        debugFileLog.exception('Exception received in download_all_screens_media')
        debugFileLog.exception(e)
