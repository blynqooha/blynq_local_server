import json
import os

import requests
from django.http import JsonResponse
from django.shortcuts import render
from django.db import transaction

# Create your views here.
from django.views.decorators.csrf import csrf_exempt

from customLibrary.settings import ACTIVATION_URL, RELATIVE_DIRECTORIES, STATS_URL
from customLibrary.storage_utils import device_key_exists, get_last_received
from downloadManagement.views import trigger_necessary_downloads
from localServer import debugFileLog
from serverBridge.views import download_screen_data
from serverBridge.helpers import string_to_dict, write_player_logs_to_file


def index(request, device_key):
    """
    :param request:
    :param device_key:
    :return: Either template index.html if the screen is already registered and registrationMsg.html if the screen is
     not registered on the blynq portal yet
    """
    debugFileLog.info('inside view index %s' % device_key)
    context_dic = {'device_key': device_key}
    if not device_key_exists(device_key):
        try:
            response = requests.post(ACTIVATION_URL, json=context_dic)
            if response.status_code == 200:
                json_content = json.loads(response.content)
                if not json_content['success']:
                    return render(request, 'registrationMsg.html', context_dic)
        except Exception as e:
            debugFileLog.exception('Exception while checking for device_key validity %s ' % device_key)
            debugFileLog.exception(e)
    return render(request, 'index.html', context_dic)


def post_to_blynq(request, url):
    try:
        response = requests.post(url=url, json=json.loads(request.body))
        if response.status_code == 200:
            json_content = string_to_dict(response.content)
            return JsonResponse(json_content, safe=False)
        else:
            debugFileLog.error("Received status code %d, reason %s" % (response.status_code, response.reason))
    except Exception as e:
        debugFileLog.exception('Received exception while fetching list of urls \n %s \n check internet connection' % str(e))
    return JsonResponse({'success': False}, safe=False)


@csrf_exempt
def activation_key_valid(request):
    debugFileLog.info('inside view activation_key_valid, posting to %s' % ACTIVATION_URL)
    return post_to_blynq(request, ACTIVATION_URL)


@csrf_exempt
def media_stats(request):
    debugFileLog.info('inside view media_stats, posting to %s' % STATS_URL)
    return post_to_blynq(request, STATS_URL)


@csrf_exempt
def handle_screen_data(request):
    json_data = {}
    try:
        posted_data = json.loads(request.body)
        device_key = posted_data.get('device_key')
        last_received = get_last_received(device_key)
        debugFileLog.info('inside view handle_screen_data with device_key %s and last_received %s' % (
            device_key, str(last_received)))
        screens_data_dir = RELATIVE_DIRECTORIES['screens_data_dir']
        file_path = os.path.join(screens_data_dir, device_key)
        if not os.path.exists(file_path):
            debugFileLog.info('get_screen_data for device_key %s does not already exist' % device_key)
            download_screen_data(device_key, last_received=last_received)
        if os.path.exists(file_path):
            with open(file_path) as f:
                json_str = f.read().replace('\n', '')
            json_data = json.loads(json_str)
            trigger_necessary_downloads(json_data)
        else:
            debugFileLog.error('get_screen_data for device_key %s is not getting downloaded ' % device_key)
    except Exception as e:
        debugFileLog.exception(e)
    return JsonResponse(json_data, safe=False)


@csrf_exempt
def insert_logs(request):
    try:
        posted_data = json.loads(request.body)
        device_key = posted_data.get('device_key')
        debugFileLog.info('inside view insert_logs with device_key %s' % device_key)
        log_str = posted_data.get('log_text')
        if device_key or log_str:
            write_player_logs_to_file(json_str=log_str, device_key=device_key)
            return JsonResponse({'success': True}, safe=False)
        else:
            device_key_str = device_key if device_key else 'No device_key'
            debugFileLog.error('Error while receiving device_key %s or logs from player' % device_key_str)
    except Exception as e:
        debugFileLog.exception('Exception while doing insert_logs')
        debugFileLog.exception(e)
    return JsonResponse({'success': False}, safe=False)
