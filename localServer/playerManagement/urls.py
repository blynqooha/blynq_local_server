from django.conf.urls import url
from playerManagement import views

urlpatterns = [
    url(r'^activationKeyValid', views.activation_key_valid, name='activation_key_valid'),
    url(r'^getScreenData', views.handle_screen_data, name='get_screen_data'),
    url(r'^mediaStats', views.media_stats, name='media_stats'),
    url(r'^logs', views.insert_logs, name='insert_logs')
]
